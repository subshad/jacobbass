###
# Page options, layouts, aliases and proxies
###

###
# Helpers
###

# middleman-syntax
activate :syntax
set :markdown_engine, :kramdown

# middleman-search-engine-sitemap
set :url_root, 'http://www.jacobbass.net'
activate :search_engine_sitemap

activate :blog do |blog|
  blog.permalink = "blog/:year/:month/:day/:title.html"
  blog.sources = "blog/posts/:year-:month-:day-:title.html"
  blog.tag_template = "blog/tag.html"
  blog.paginate = true
end

page "/blog/feed.xml", :layout => false

activate :drafts

# Reload the browser automatically whenever files change
configure :development do
  activate :livereload
end

activate :bootstrap_navbar

after_configuration do
  sprockets.append_path File.join root, 'bower_components'
end

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end

set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'

# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Enable cache buster
  activate :asset_hash

  # Use relative URLs
  activate :relative_assets

  # Or use a different image path
  # set :http_prefix, "/Content/images/"
end

activate :deploy do |deploy|
  deploy.method = :sftp
  deploy.build_before = true
  deploy.user     = 'j'
  deploy.host     = '45.55.213.199'
  deploy.port     = 44928
  deploy.path     = '/var/www/jacobbass.net/public_html'
end

#Filewatcher ignore list
set :file_watcher_ignore,[
    /^bin(\/|$)/,
    /^\.bundle(\/|$)/,
#        /^vendor(\/|$)/,
    /^node_modules(\/|$)/,
    /^\.sass-cache(\/|$)/,
    /^\.cache(\/|$)/,
    /^\.git(\/|$)/,
    /^\.gitignore$/,
    /\.DS_Store/,
    /^\.rbenv-.*$/,
    /^Gemfile$/,
    /^Gemfile\.lock$/,
    /~$/,
    /(^|\/)\.?#/,
    /^tmp\//
]
