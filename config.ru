require 'rubygems'
require 'middleman'

require 'rack/contrib/try_static'
require 'rack/rewrite'

run Middleman::Application.server

use Rack::Rewrite do
  r301 %r{^/(.*)/$}, '/$1'
end

use Rack::Deflater
use Rack::TryStatic,
  root: 'build',
  urls: %w[/],
  try: %w[.html index.html /index.html]

FIVE_MINUTES=300

run lambda { |env|
  [
    404,
    {
      'Content-Type'  => 'text/html',
      'Cache-Control' => "public, max-age=#{FIVE_MINUTES}"
    },
    ['a File not found']
  ]
}
