var toggleBlogFilter = function() {
  var toggleSwitch = document.getElementById("blog-filter-toggle-link");
  var tagDiv = document.getElementById("blog-tag-filter");
  var dateDiv = document.getElementById("blog-date-filter");

  if (toggleSwitch.innerHTML == "Show()") {
    tagDiv.style.display = "block";
    dateDiv.style.display = "block";
    toggleSwitch.innerHTML = "Hide()";
  } else {
    tagDiv.style.display = "none";
    dateDiv.style.display = "none";
    toggleSwitch.innerHTML = "Show()";
  }
}

var inject_blog_tag = function() {
  var sPath = window.location.pathname;
  if (sPath.indexOf("/tags/") != -1) {
    var sPage = sPath.substring(sPath.lastIndexOf('/') + 1, sPath.lastIndexOf('.'));
    document.getElementById("title").innerHTML += sPage;
  }
}

var sPath = window.location.pathname;
var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);

if (sPage == 'blog') {
  window.onload = function() {
    var toggleSwitch = document.getElementById("blog-filter-toggle-link");
    var tagDiv = document.getElementById("blog-tag-filter");
    var dateDiv = document.getElementById("blog-date-filter");
  
    tagDiv.style.display = "none";
    dateDiv.style.display = "none";
    toggleSwitch.innerHTML = "Show()";
  }
} else if(sPath.indexOf("/tags/") != -1) {
  window.onload = function() {
    inject_blog_tag();
  }
}
