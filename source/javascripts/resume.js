var sPath = window.location.pathname;
var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);

if (sPage == 'resume') {
  window.onload = function() {
    var subtitle = document.getElementById("subtitle");
    var docx_link = '<a href="assets/jacobbass-resume.docx">.docx</a>'
    var pdf_link = '<a href="assets/jacobbass-resume.pdf">.pdf</a>'

    subtitle.innerHTML = subtitle.innerHTML.replace(".docx",docx_link)
    subtitle.innerHTML = subtitle.innerHTML.replace(".pdf",pdf_link)
  }
}
