---
title: "About Me"
subtitle: "Everything you might want to know..."
description: "Hi, I'm Jacob. I'm an engineer who focuses in back-end web development. I have years of experience working in and leading medium to large teams, as well as a proven track record of delivering efficient solutions to interesting problems."
---

## Hi, I'm Jacob.

I'm an engineer who focuses in back-end web development. 
I have years of experience working in and leading medium to large teams, as well 
as a proven track record of delivering efficient solutions to interesting problems.

## I'm an Electrical Engineer.

I graduated with a Bachelor's Degree in Electrical Engineering (Hons.) before
beginning work in a software development company. I wrote an Honour's Thesis on Software Defined Networking,
working with a professor in the Telecommunications faculty. For the thesis I developed a testbed and demonstration system
to help improve the bandwidth and performance for home users in areas with lower quality connections.

## I develop software.

I have worked as a software developer, software manager, MVP developer 
as well as managing non-technical members of the team through QA and IT roles.

## I play music.

I love playing guitar, from Segovia to Vai. I'm slowly trying to work my way through 
the music archived at [Classical Guitar Tablature](http://www.classtab.org).

## I read as much as I can.

Since I first picked up [Foundation](http://en.wikipedia.org/wiki/Foundation_%28novel%29) at age 10 I've been
reading every bit of science fiction I could get my hands on. I love Asimov, Heinlein, Clarke, Pratchett, Tolkein
and everything else I can find. I also try to learn about science, psychology and mathematics. I love learning new things
and extending my knowledge.
