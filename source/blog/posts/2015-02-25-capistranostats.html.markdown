---
title: 'How To: Disable Stats Prompt During Capistrano Deploy'
date: 2015-02-25 11:35 UTC
tags: 'capistrano,devops'
---

It's 5 o'clock on a Friday afternoon, and against your better judgement you've
decided to deploy to staging, production or your demo server for a weekend
demonstration.

Your version of Capistrano has updated recently (You've assumed that pinning it
to a middle integer semver build will be safe) and you're using the bleeding
edge versions of of the rails integration gems. What could go wrong?

You're using Buildkite, or TravisCI and using the continuous deployment
workflow. And you see your deploy failing. Over and over and over again.

~~~
  Would you like to enable statistics? Here is an example message we would
  send:

  1|2015-02-17T04:43:54+00:00|2.1.3|x86_64-linux|3.3.5|421f9bf9

  Do you want to enable statistics? (y/N): (Backtrace restricted to imported
  tasks)
  cap aborted!
  SignalException: SIGHUP

  Tasks: TOP => metrics:collect
  (See full trace by running task with --trace)
  23The deploy has failed with an error: SIGHUP
~~~

This is a regression caused by the Capistrano/Stats gem (at the time of this
post last updated on Dec. 13 2014, version 1.1.1). Our first clue is the
blocking question

> Do you want to enable statistics? (y/N):

This operation will obviously hang forever, causing your build to timeout and
fail. The docs for Capistrano stats or Capistrano don't have any information
about this failure nor any exposed configuration options for how to deal with
it. So we have to dig a little to figure out how to fix this.

If we visit the [Capistrano Stats github page](https://github.com/capistrano/stats),
we can try and drill down through the source code to try and figure out
how to disable this prompt. First we search for the phrase 'Do you want to
enable statistics?'. This leads us to the
[MetricCollector Class](https://github.com/capistrano/stats/blob/ce1d56cfe8dee0e02de3dae08248744ad0a2ec16/gem/lib/capistrano-stats/metric-collector.rb)
which has the method
[#ask_to_enable](https://github.com/capistrano/stats/blob/ce1d56cfe8dee0e02de3dae08248744ad0a2ec16/gem/lib/capistrano-stats/metric-collector.rb#L67)
which prompts the user (blocking, with no timeout). When the user has answered,
the result is passed to the
[#write_setting](https://github.com/capistrano/stats/blob/ce1d56cfe8dee0e02de3dae08248744ad0a2ec16/gem/lib/capistrano-stats/metric-collector.rb#L115) method.
The result returned by answering the prompt with yes is the symbol
:full. If the user answers no, a following question is asked. If the user then
response yes the result is :ping, otherwise the result returned is :none. :ping
will send an anonymised statistics dump to the Capistrano remote server (for
their analytics). :full seems to skip the anonymisation step, sending your full
information (possibly a security risk). :none will skip this collection
entirely.

The [#write_setting](https://github.com/capistrano/stats/blob/ce1d56cfe8dee0e02de3dae08248744ad0a2ec16/gem/lib/capistrano-stats/metric-collector.rb#L115)
method contains a reference to a sentry_file, where it writes the value selected
by the user. By looking at the 
[#sentry_file](https://github.com/capistrano/stats/blob/ce1d56cfe8dee0e02de3dae08248744ad0a2ec16/gem/lib/capistrano-stats/metric-collector.rb#L105)
method we see that our git repo needs to check in a file located at

> $GIT_REPO_BASE/.capistrano/metrics

This file should contain whichever of the preferences you laid out above,
written as a symbol. So if you would like no tracking whatsoever, you would
choose :none.

The [#user_preference](https://github.com/capistrano/stats/blob/ce1d56cfe8dee0e02de3dae08248744ad0a2ec16/gem/lib/capistrano-stats/metric-collector.rb#L15)
method shows that the blocking prompt will be disabled by the existance of the
.capistrano/metrics file. However if the file does not contain your preference
it will default to anonymised statitistics collection.

Now the failing deploy part of this annoying regression could be fixed by your
chosen CI/CD server not using a TTY session to deploy, which would disable the
prompt.
