---
title: "How to: Install NeoVim alongside Vim on OSX"
description: "How to install NeoVim alongside Vim on OSX, using the same .vimrc and .vim between both applications"
date: 2015-01-16 17:03 UTC
tags: neovim
---

I've been using Vim for years now without paying very much attention to the community,
newsgroups or anything. Short of trying to decide between [YCM](https://github.com/Valloric/YouCompleteMe) 
and [NeoComplete](https://github.com/Shougo/neocomplete.vim), 
(Neither, too much overhead for the system I was using at the time), I more or less used it with a set and forget
git repository ([shameless plug](https://github.com/subshad/.vim)).

I recently read [this post](http://geoff.greer.fm/2015/01/15/why-neovim-is-better-than-vim/) without very much context,
prompting some searching. I've decided to give NeoVim a test alongside Vim to decide whether the improvement
is tangible for users outside of more and newer plugins due to the revamped plugin system.

However, the [wiki](https://github.com/neovim/neovim/wiki) and the [documentation](http://neovim.org/doc/) 
didn't list a complete set of installation instructions as far as I'm concerned. From the wiki:

~~~Bash
  brew tap neovim/homebrew-neovim
  brew install --HEAD neovim
~~~

This doesn't include information on how to access your existing Vim configuration and settings. To use the same
.vimrc and .vim folder, you can create symlinks to your existing files in your home folder (or wherever you store them). 
As always, please backup your files and folders before doing anything, you wouldn't want to copy a typo and overwrite all
your configuration files...

~~~Bash
  cd ~
  ln -s .vimrc .nvimrc
  ln -s .vim .nvim
~~~

That's it. Provided you don't have any configuration options set in your .vimrc that aren't 
implemented in NeoVim (or vice-versa) then you'll be able to use Vim and NeoVim side by side on OSX.
Short the OSX specific installation information above, this symlink method should work on
other Unix OSes as well.
