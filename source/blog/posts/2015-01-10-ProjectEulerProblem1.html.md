---
title: "Project Euler: Problem 1"
subtitle: "Fizz Buzz by any other name"
tags: project_euler,coding
date: 2015-01-10 21:39 UTC
description: "A teardown solution of the first project euler problem."
---

The [maiden problem](https://projecteuler.net/problem=1) from Project Euler is a 
version of the classic Fizz Buzz cliche problem, with a solution criteria of a sum rather
than printing Fizz, Buzz and FizzBuzz.

The problem states:  

> If we list all the natural numbers below 10 that are multiples of 3 or 5, 
> we get 3, 5, 6 and 9. The sum of these multiples is 23.  
> Find the sum of all the multiples of 3 or 5 below 1000.

A brute force approach to this problem can be structured in the form:

~~~
  for every integer from 1 to limit
    check if it is divisible by 3
      if it is, add it to the sum
    otherwise, check if it is 5
      if it is, add it to the sum
    otherwise do nothing
  done
~~~

Written in Ruby:

~~~ruby
  (1..999).select { |n| ((n%3==0)||(n%5==0)) }.inject { |sum, n| sum + n }
~~~

Alternatively, you could iterate over each of the multiples of each constraing (3,5)
and sum them, subtracting the sum of the multiples of the LCM.

~~~
  Add to the sum all the numbers below the limit that are multiples of three
  Add to the sum all the numbers below the limit that are mulitiples of five
  Subtract from the sum all the numbers below the limit that are multiples of three and five
~~~

Written in Ruby:

~~~ruby
  sum = 0
  (1..(999/3)).each { |n| sum = sum + 3*n }
  (1..(999/5)).each { |n| sum = sum + 5*n }
  (1..(999/15)).each { |n| sum = sum - 15*n }
~~~

This algorithm saves on iterating over ~250 values over the previous algorithm, a saving of 25%. It also doesn't
utilise division, which is a costly operation, as compared to sums and multiplication.

Using the mathematical property for the sum of a series, this reduces even further down to

~~~
  3*sum(1 to limit/3) + 5*sum(1 to limit/5) - 15*sum(1 to limit/15)
  where sum(1 to limit/n) = (limit/2n)*(1 + limit/n)
~~~

Written in Ruby:

~~~Ruby
  3*333*167 + 5*199*100 - 15*33*67
~~~

This is about as simple as it gets for this problem. 
The brute force method requires 1000 iterations and 533 divisions and comparisons.
The best algorithm presented here requires 6 multiplications.
