---
title: Hello World
description: "My first blog post."
tags: other
---

In the same vein as the first program written to learn a language, this post is intended as a hello world for this blog.

And so...

~~~c
  void firstBlogPost() {
    printf("Hello World!");
  }
~~~
