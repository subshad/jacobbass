---
title: Home
subtitle: The personal page of Jacob Bass
description: "The personal page of Jacob Bass"
---

## I'm Jacob.

I'm an Electrical Engineering graduate of UNSW who enjoys problem solving,
puzzles and technology. When these intersect, it's very hard to distract me. I have been
using and instructing computers for years, with a recent focus on Ruby on Rails and web development.

## I like to learn how things work.

To learn a little more about me, please visit the [about me](about) page.

## I like to build, develop and improve.

If you're interested in some of the work I've done in the past, please check my [resume](resume). I'll also be
adding a projects page in the near future to showcase some of my work.

## I'd like to speak to you!

If you would like to communicate with me, please [reach out](mailto:jacob@jacobbass.net) using one of the options in the menu. I'm eager to discuss
technology and opportunities at any time.