set :application, 'jacobbass.net'
set :use_sudo, false
set :build_dir, "public_html"
set :ssh_login, "j@jacobbass.net"
set :deploy_dir, "/var/www/#{fetch(:application)}"

namespace :middleman do
  desc "Build and deploy middleman project"
  task :deploy do
    invoke 'middleman:build'
    invoke 'middleman:deploy'
  end

  desc "Run build utility"
  task :build do
    run_locally do
     execute "middleman build"
    end
  end

  desc "Deploy to VPS"
  task :deploy do
    run_locally do
     execute "scp -r #{fetch(:build_dir)} #{fetch(:ssh_login)}:#{fetch(:deploy_dir)}"
    end
  end
end
